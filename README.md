# Responsive Video Background Landing Page

This is a very simple responsive landing page with a video background made using HTML5, and CSS3.

# Live Demo:
https://ubden.github.io/demoVideolandingpage
## Built With

* HTML5
* CSS3

## Acknowledgement

* This project was inspired by Traversy Media. I would highly recommend anyone interested in development to check out his YouTube channel
as he has tutorials on all the latest technologies.
